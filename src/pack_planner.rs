use std::convert::TryInto;
use std::error::Error;
use std::io::BufRead;

use crate::models::{Pack, PackItem, PackItemConfig, SortOrder};

pub type Result<T> = std::result::Result<T, Box<dyn Error>>;

pub fn plan<R: BufRead>(reader: R) -> String {
    match parse_input_data(reader) {
        Ok((c, i)) => create_report(assemble_packs(c, i)),
        Err(e) => print_error_report(e)
    }
}

/// Parses the input data into the ```config``` and ```packetInputData```
fn parse_input_data<R: BufRead>(mut reader: R) -> Result<(PackItemConfig, Vec<PackItem>)> {
    let config = &pack_item_config(&mut reader)?;
    Ok((config.clone(), sort_items(packet_input_data(&mut reader)?, config)))
}

///Takes the sorted  packet input data and distributes it over packs as per the maximum weight and -quantity requirements.
///The result is a List of Packs
fn assemble_packs(config: PackItemConfig, pack_items: Vec<PackItem>) -> Vec<Pack> {
    let mut packs = Vec::<Pack>::default();
    let mut pack = Pack::new(1);

    pack_items.iter().cloned().for_each(|mut item| {
        while !item.is_empty() {
            item = pack.fill_with(&item, &config);

            //When the returned item is NOT empty, the pack has been filled to max capacity and is to be stored in the result...
            if !item.is_empty() {
                add_pack(&mut packs, &pack);
                pack = Pack::new(pack.pack_number + 1);
            }
        }
    });

    //Once all pack-items have been distributed over the packs, the last pack needs to be added as well...
    add_pack(&mut packs, &pack);

    //return the just-created packs...
    packs
}

///Stores the pack in the packs collection...
fn add_pack(packs: &mut Vec<Pack>, pack: &Pack) {
    packs.push(pack.clone());
}


/// Takes the List of Packs and creates a report for them.
fn create_report(packs: Vec<Pack>) -> String {
    let mut report = String::default();
    packs.iter().for_each(|pack|
        {
            report += format!("Pack Number: {}\n", pack.pack_number).as_str();
            pack.items.iter().for_each(|it|
                report += format!("{},{},{},{}\n", it.item_id, it.length, it.quantity, it.weight).as_str()
            );

            report += format!("Pack Length: {}, Pack Weight: {}\n", pack.length(), (pack.total_weight() * 1000_f32).round() as u32 as f32 / 1000.0).as_str();
            report += "\n"
        }
    );
    report.trim().to_owned()
}

///The error will become the report
fn print_error_report(e: Box<dyn Error>) -> String {
    format!("An error has happened: {}", e)
}

///Sort the packet input data as per the sort-order
fn sort_items(mut packet_input_data: Vec<PackItem>, config: &PackItemConfig) -> Vec<PackItem> {
    match &config.sort_order {
        SortOrder::ShortToLong => packet_input_data.sort_by(|a, b| a.length.cmp(&b.length)),
        SortOrder::LongToShort => packet_input_data.sort_by(|a, b| b.length.cmp(&a.length)),
        _ => {}
    };
    packet_input_data
}

/// Parses input lines 2 and beyond into Packet-Items; Parsing is done when a blank line is encountered.
/// e.g:
/// ```
/// 1001,6200,30,9.653
/// 2001,7200,50,11.21
///
/// ```
/// returns the parsed data
fn packet_input_data<R: BufRead>(reader: &mut R) -> Result<Vec<PackItem>> {
    let mut packet_input_data = Vec::<PackItem>::default();
    let mut line = next_line(reader)?;
    while !end_of_input(&line) {
        packet_input_data.push(parse_line(line)?);
        line = next_line(reader)?
    }
    Ok(packet_input_data)
}


///Tries to parse the line-string into a PackItem. When successful, this Pack-item is added to the list.
///Returns an error when the line is not a CSV-string with four elements comprising of valid data
fn parse_line(line: String) -> Result<PackItem> {
    line.split(',').collect::<Vec<&str>>().try_into()
}

///Parses the first input line into a Config-Object
///e.g:
///```
///NATURAL,40,500.0
///```
///returns The PacketIem Config or an error when the line is not a CSV-string with three elements valid data
fn pack_item_config<R: BufRead>(reader: &mut R) -> Result<PackItemConfig> {
    next_line(reader).unwrap().split(',').collect::<Vec<&str>>().try_into()
}

fn next_line<R: BufRead>(reader: &mut R) -> Result<String> {
    let mut input = String::new();
    reader.read_line(&mut input)?;
    Ok(input)
}

fn end_of_input(line: &str) -> bool {
    line.trim().is_empty()
}