use std::io;

pub mod models;
pub mod pack_planner;


fn main() {
    println!("{}",pack_planner::plan(io::stdin().lock()));
}

#[cfg(test)]
mod tests;
