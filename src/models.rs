use std::cmp::{max, min};
use std::convert::TryFrom;
use std::error::Error;
use std::fmt::Debug;
use std::str::FromStr;

use crate::pack_planner::Result;
use crate::models::SortOrder::Natural;

/**
 * The input data sort-options
 */
#[derive(Debug, Clone, Eq, PartialEq)]
pub enum SortOrder { Natural, ShortToLong, LongToShort }

impl FromStr for SortOrder {
    type Err = Box<dyn Error>;

    fn from_str(s: &str) -> Result<Self> {
        let no_underscores = s.replace('_', "").to_lowercase();
        match no_underscores.as_str() {
            "natural" => Ok(SortOrder::Natural),
            "shorttolong" => Ok(SortOrder::ShortToLong),
            "longtoshort" => Ok(SortOrder::LongToShort),
            _ => Err(format!(r#"Could not convert '{}' into a SortOrder: Valid sort-orders are "NATURAL", "SHORT_TO_LONG" and "LONG_TO_SHORT", "#, s).into()),
        }
    }
}

impl Default for SortOrder {
    fn default() -> Self {
        Natural
    }
}

/**
 * The First line of the Input data
 */
#[derive(Default, Debug, Clone, PartialEq)]
pub struct PackItemConfig {
    pub sort_order: SortOrder,
    pub max_quantity: u32,
    pub max_weight: f32,
}

/**
 * Lines 2 and up of the input-data
 */
#[derive(Default, Debug, Clone, PartialEq)]
pub struct PackItem {
    pub item_id: u64,
    pub length: u32,
    pub quantity: u32,
    pub weight: f32,
}

/**
 * A collection of PackItems;
 * It can take PackItem and return the portion that exceeds quantity- and/or weight restrictions;
 * this remainder can then be used to stack other packs.
 */
#[derive(Default, Debug, Clone)]
pub struct Pack { pub items: Vec<PackItem>, pub pack_number: u32 }

impl Pack {
    /// returns a new empty Pack with the pack-number provided
    pub fn new(pack_number: u32) -> Self {
        Pack { items: vec!(), pack_number }
    }

    pub fn length(&self) -> u32 { self.items.iter().fold(0, |acc, item| max::<u32>(acc, item.length)) }

    pub fn total_weight(&self) -> f32 { self.items.iter().fold(0.0, |acc, item| acc + item.weight * item.quantity as f32) }

    pub fn total_quantity(&self) -> u32 { self.items.iter().fold(0, |acc, item| acc + item.quantity) }

    /**
     * Take all the content of the packItem that will fit into the pack, return the remainder
     */
    pub fn fill_with(&mut self, item: &PackItem, config: &PackItemConfig) -> PackItem {

        //Consider only items that have quantity AND weight...
        if item.is_empty() {
            return PackItem { item_id: item.item_id, length: item.length, quantity: 0, weight: 0.0 };
        }

        //The quantity that can be added, not exceeding the weight limitations...
        let max_take_quantity_by_weight = ((config.max_weight - self.total_weight()) / item.weight) as u32;

        //The quantity that could still be added, ignoring weight limitations...
        let remaining_quantity = config.max_quantity - self.total_quantity();

        //The maximum quantity within the bounds of both maximum-weight and maximum-overall-quantity...
        let max_take_quantity = min(max_take_quantity_by_weight, remaining_quantity);

        //Only continue when the pack is not already filled to a max ...
        if max_take_quantity <= 0 {
            return item.clone();
        }

        //Take the whole lot...
        return if max_take_quantity > item.quantity {
            self.items.push(item.clone());
            PackItem { item_id: item.item_id, length: item.length, quantity: 0, weight: item.weight }
        }
        //Take all the quantity that is allowed; return the items that could not be fitted...
        else {
            self.items.push(PackItem { item_id: item.item_id, length: item.length, quantity: max_take_quantity, weight: item.weight });
            PackItem { item_id: item.item_id, length: item.length, quantity: item.quantity - max_take_quantity, weight: item.weight }
        };
    }
}

impl PackItem {
    pub fn is_empty(&self) -> bool { self.quantity <= 0 || self.weight <= 0.0 }
}

impl TryFrom<Vec<&str>> for PackItem {
    type Error = Box<dyn Error>;

    fn try_from(rd: Vec<&str>) -> Result<Self> {
        if rd.len() != 4 {
            return Err(r#"The Items lines MUST have format "[item id],[item length],[item quantity],[piece weight]" e.g. "1001,6200,30,9.653""#.into());
        }

        Ok(PackItem {
            item_id: rd[0].trim().parse::<u64>()?,
            length: rd[1].trim().parse::<u32>()?,
            quantity: rd[2].trim().parse::<u32>()?,
            weight: rd[3].trim().parse::<f32>()?,
        })
    }
}

impl TryFrom<Vec<&str>> for PackItemConfig {
    type Error = Box<dyn Error>;

    fn try_from(conf: Vec<&str>) -> Result<Self> {
        if conf.len() != 3 {
            return Err(r#"The first Line MUST have format "[Sort order], [max pieces per pack], [max weight per pack]" e.g: "NATURAL, 40, 500.0""#.into());
        }

        Ok(PackItemConfig {
            sort_order: conf[0].trim().parse::<SortOrder>()?,
            max_quantity: conf[1].trim().parse::<u32>()?,
            max_weight: conf[2].trim().parse::<f32>()?,
        })
    }
}