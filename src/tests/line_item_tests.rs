use std::convert::TryInto;
use std::error::Error as StdError;

use crate::models::{PackItem};

pub type Result<T> = std::result::Result<T, Box<dyn StdError>>;

#[test]
fn parse_ok_test() {
    let line_data = vec!("1001", "6200", "30", "9.653");
    let item: Result<PackItem> = line_data.try_into();
    assert_eq!(item.unwrap(), PackItem { item_id: 1001, length: 6200, quantity: 30, weight: 9.653 });
}

#[test]
fn only_three_items_test() {
    let line_data = vec!("6200", "30", "9.653");
    let item: Result<PackItem> = line_data.try_into();
    assert_eq!(format!("{}", item.unwrap_err()), r#"The Items lines MUST have format "[item id],[item length],[item quantity],[piece weight]" e.g. "1001,6200,30,9.653""#.to_owned());
}

#[test]
fn incorrect_numeric_items_test() {
    let line_data = vec!("1001.0", "6200.0", "30", "9.653");
    let config: Result<PackItem> = line_data.try_into();
    assert_eq!(config.unwrap_err().to_string(), "invalid digit found in string".to_owned());
}

#[test]
fn non_numeric_items_test() {
    let line_data = vec!("Bibbidi", "Babbidi", "Boo", "9.653");
    let config: Result<PackItem> = line_data.try_into();
    assert_eq!(config.unwrap_err().to_string(), "invalid digit found in string".to_owned());
}