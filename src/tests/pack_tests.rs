
use crate::models::{Pack, PackItem, PackItemConfig, SortOrder};

#[test]
fn total_weight_test() {
    let mut pack: Pack = Pack { items: vec!(PackItem { item_id: 1001, length: 6200, quantity: 30, weight: 9.6 }), pack_number: 1 };
    assert_eq!(pack.total_weight(), 288.0);
    pack.items.push(PackItem { item_id: 2001, length: 7200, quantity: 50, weight: 11.21 });
    assert_eq!(pack.total_weight(), 848.5);
    pack.items.push(PackItem { item_id: 2001, length: 7200, quantity: 14, weight: 22.20 });
    assert_eq!(pack.total_weight(), 1159.3);
}

#[test]
fn total_quantity_test() {
    let mut pack: Pack = Pack { items: vec!(), pack_number: 1 };
    assert_eq!(pack.total_quantity(), 0);
    pack.items.push(PackItem { item_id: 1001, length: 6200, quantity: 30, weight: 9.653 });
    assert_eq!(pack.total_quantity(), 30);
    pack.items.push(PackItem { item_id: 2001, length: 7200, quantity: 50, weight: 11.21 });
    assert_eq!(pack.total_quantity(), 80);
    pack.items.push(PackItem { item_id: 2001, length: 7200, quantity: 14, weight: 22.20 });
    assert_eq!(pack.total_quantity(), 94);
}

#[test]
fn length_test() {
    let mut pack: Pack = Pack { items: vec!(), pack_number: 1 };
    assert_eq!(pack.length(), 0);
    pack.items.push(PackItem { item_id: 1001, length: 6200, quantity: 30, weight: 9.653 });
    assert_eq!(pack.length(), 6200);
    pack.items.push(PackItem { item_id: 2001, length: 7200, quantity: 50, weight: 11.21 });
    assert_eq!(pack.length(), 7200);
    pack.items.push(PackItem { item_id: 2001, length: 8200, quantity: 14, weight: 22.20 });
    assert_eq!(pack.length(), 8200);
}

#[test]
fn fill_with_maximum_item_count_test() {
    let config = &PackItemConfig { sort_order: SortOrder::Natural, max_quantity: 100, max_weight: 1000.0 };
    let mut pack: Pack = Pack { items: vec!(), pack_number: 1 };
    assert_eq!(pack.total_quantity(), 0);
    assert_eq!(pack.total_weight(), 0.0);
    assert_eq!(pack.length(), 0);
    assert_eq!(pack.fill_with(&PackItem { item_id: 1001, length: 6200, quantity: 30, weight: 2.0 }, config), PackItem { item_id: 1001, length: 6200, quantity: 0, weight: 2.0 });
    assert_eq!(pack.total_quantity(), 30);
    assert_eq!(pack.total_weight(), 60.0);
    assert_eq!(pack.length(), 6200);
    assert_eq!(pack.fill_with(&PackItem { item_id: 1002, length: 6200, quantity: 30, weight: 0.8 }, config), PackItem { item_id: 1002, length: 6200, quantity: 0, weight: 0.8 });
    assert_eq!(pack.total_quantity(), 60);
    assert_eq!(pack.total_weight(), 84.0);
    assert_eq!(pack.length(), 6200);
    assert_eq!(pack.fill_with(&PackItem { item_id: 1003, length: 7200, quantity: 50, weight: 70.5 }, config), PackItem { item_id: 1003, length: 7200, quantity: 38, weight: 70.5 });
    assert_eq!(pack.total_quantity(), 72);
    assert_eq!(pack.total_weight(), 930.0);
    assert_eq!(pack.length(), 7200);
    assert_eq!(pack.fill_with(&PackItem { item_id: 1004, length: 8200, quantity: 100, weight: 0.6 }, config), PackItem { item_id: 1004, length: 8200, quantity: 72, weight: 0.6 });
    assert_eq!(pack.total_quantity(), 100);
    assert_eq!(pack.total_weight(), 946.8);
    assert_eq!(pack.length(), 8200);
}

#[test]
fn fill_with_maximum_weight_test() {
    let config = &PackItemConfig { sort_order: SortOrder::Natural, max_quantity: 100, max_weight: 1000.0 };
    let mut pack: Pack = Pack { items: vec!(), pack_number: 1 };
    assert_eq!(pack.total_quantity(), 0);
    assert_eq!(pack.total_weight(), 0.0);
    assert_eq!(pack.length(), 0);
    assert_eq!(pack.fill_with(&PackItem { item_id: 1001, length: 7200, quantity: 30, weight: 10.0 }, config), PackItem { item_id: 1001, length: 7200, quantity: 0, weight: 10.0 });
    assert_eq!(pack.total_quantity(), 30);
    assert_eq!(pack.total_weight(), 300.0);
    assert_eq!(pack.length(), 7200);
    assert_eq!(pack.fill_with(&PackItem { item_id: 1002, length: 6200, quantity: 25, weight: 30.0 }, config), PackItem { item_id: 1002, length: 6200, quantity: 2, weight: 30.0 });
    assert_eq!(pack.total_quantity(), 53);
    assert_eq!(pack.total_weight(), 990.0);
    assert_eq!(pack.length(), 7200);
    assert_eq!(pack.fill_with(&PackItem { item_id: 1003, length: 6200, quantity: 3, weight: 1.0}, config), PackItem { item_id: 1003, length: 6200, quantity: 0, weight: 1.0 });
    assert_eq!(pack.total_quantity(), 56);
    assert_eq!(pack.total_weight(), 993.0);
    assert_eq!(pack.length(), 7200);
    assert_eq!(pack.fill_with(&PackItem { item_id: 1004, length: 8200, quantity: 11, weight: 1.0 }, config), PackItem { item_id: 1004, length: 8200, quantity: 4, weight: 1.0 });
    assert_eq!(pack.total_quantity(), 63);
    assert_eq!(pack.total_weight(), 1000.0);
    assert_eq!(pack.length(), 8200);

    //Not to be added...
    assert_eq!(pack.fill_with(&PackItem { item_id: 1003, length: 6200, quantity: 3, weight: 30.0 }, config), PackItem { item_id: 1003, length: 6200, quantity: 3, weight: 30.0 });
    assert_eq!(pack.total_quantity(), 63);
    assert_eq!(pack.total_weight(), 1000.0);
    assert_eq!(pack.length(), 8200);
    assert_eq!(pack.fill_with(&PackItem { item_id: 1004, length: 6200, quantity: 0, weight: 0.0 }, config), PackItem { item_id: 1004, length: 6200, quantity: 0, weight: 0.0 });
    assert_eq!(pack.total_quantity(), 63);
    assert_eq!(pack.total_weight(), 1000.0);
    assert_eq!(pack.length(), 8200);
    assert_eq!(pack.fill_with(&PackItem { item_id: 1004, length: 6200, quantity: 10, weight: -100.0 }, config), PackItem { item_id: 1004, length: 6200, quantity: 0, weight: 0.0 });
    assert_eq!(pack.total_quantity(), 63);
    assert_eq!(pack.total_weight(), 1000.0);
    assert_eq!(pack.length(), 8200);

    //The Pack-contents...
    assert_eq!(format!("{:?}",pack).as_str(), "Pack { items: [PackItem { item_id: 1001, length: 7200, quantity: 30, weight: 10.0 }, PackItem { item_id: 1002, length: 6200, quantity: 23, weight: 30.0 }, PackItem { item_id: 1003, length: 6200, quantity: 3, weight: 1.0 }, PackItem { item_id: 1004, length: 8200, quantity: 7, weight: 1.0 }], pack_number: 1 }");
}