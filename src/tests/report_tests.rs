use std::fs::File;
use std::io::BufReader;
use std::io::prelude::*;

use crate::pack_planner::plan;

#[test]
fn natural_ordering_test() {
    assert_eq!(plan(file_buf_reader("src/tests/data/natural.txt")), expected_report("src/tests/data/natural_report.txt"));
}

#[test]
fn long_to_short_ordering_test() {
    assert_eq!(plan(file_buf_reader("src/tests/data/long_to_short.txt")), expected_report("src/tests/data/long_to_short_report.txt"));
}

#[test]
fn short_to_long_ordering_test() {
    assert_eq!(plan(file_buf_reader("src/tests/data/short_to_long.txt")), expected_report("src/tests/data/short_to_long_report.txt"));
}

#[test]
fn incorrect_config_test() {
    assert_eq!(plan(file_buf_reader("src/tests/data/incorrect_config.txt")), expected_report("src/tests/data/incorrect_config_report.txt"));
}

#[test]
fn incorrect_line_item_test() {
    assert_eq!(plan(file_buf_reader("src/tests/data/incorrect_line_item.txt")), expected_report("src/tests/data/incorrect_line_item_report.txt"));
}

fn file_buf_reader(test_file: &str) -> BufReader<File> {
    BufReader::new(File::open(test_file).expect("Could not read test data"))
}

fn expected_report(rep_file: &str) -> String {
    let result = &mut String::new();
    File::open(rep_file).expect("Could not open report file").read_to_string(result).expect("could not read report file");
    result.to_owned()
}