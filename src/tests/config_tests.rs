use std::convert::TryInto;
use std::error::Error as StdError;

use crate::models::{PackItemConfig, SortOrder};

pub type Result<T> = std::result::Result<T, Box<dyn StdError>>;

#[test]
fn natural_ordering_test() {
    let line_data = vec!("NATURAL", "40", "500.0");
    let config: Result<PackItemConfig> = line_data.try_into();
    assert_eq!(config.unwrap(), PackItemConfig { sort_order: SortOrder::Natural, max_quantity: 40, max_weight: 500.0 });
}

#[test]
fn only_two_items_test() {
    let line_data = vec!("SHORT_TO_LONG", "9.653");
    let config: Result<PackItemConfig> = line_data.try_into();
    assert_eq!(format!("{}", config.unwrap_err()), r#"The first Line MUST have format "[Sort order], [max pieces per pack], [max weight per pack]" e.g: "NATURAL, 40, 500.0""#.to_owned());
}

#[test]
fn incorrect_numeric_items_test() {
    let line_data = vec!("LONG_TO_SHORT", "6200.0", "9.653");
    let config: Result<PackItemConfig> = line_data.try_into();
    assert_eq!(config.unwrap_err().to_string(), "invalid digit found in string".to_owned());
}

#[test]
fn non_numeric_items_test() {
    let line_data = vec!("1001.0", "6200.0", "30");
    let config: Result<PackItemConfig> = line_data.try_into();
    assert_eq!(config.unwrap_err().to_string(), r#"Could not convert '1001.0' into a SortOrder: Valid sort-orders are "NATURAL", "SHORT_TO_LONG" and "LONG_TO_SHORT", "#.to_owned());
}