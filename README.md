
## Pack Planner
This pack planner takes a list of items and sorts them into several packs (groups).

### An Item contains
1. Item id
1. Length (mm)
1. Quantity
1. Item Weight (kg, the weight of one item)

### A Pack has:
1. Pack id
1. List of Items

### The Packs are created with one of the following criteria
1. Packing items from shortest to longest.
1. Packing items from longest to shortest.
1. Packing items in the order they were given. 
   
### Program limitations
1. Max weight allowed in any pack
1. Max items in any pack
1. Pack Sort Order
   Sort orders are: NATURAL, SHORT_TO_LONG, LONG_TO_SHORT
   Natural refers to the order they were given as input. 
   
### Special Note
1. Items can be split across packs. (5 x 1000 pieces can be split so that 2 x 1000 are in pack A and 3 x 1000 are in pack B)
1. Items are stacked one on top of the other forming a stack which is referred to as a Pack.


## Example Input and Output

```
#Input format:
[Sort order],[max pieces per pack],[max weight per pack]
[item id],[item length],[item quantity],[piece weight]
[item id],[item length],[item quantity],[piece weight]
[item id],[item length],[item quantity],[piece weight]
...

#Output format:
Pack number: [pack number]
[item id],[item length],[item quantity],[piece weight]
[item id],[item length],[item quantity],[piece weight]
...
Pack Length: [pack length], Pack Weight: [pack weight]

#STD input example: (input ends when an empty line is received or you reach the end of the input stream)
NATURAL,40,500.0
1001,6200,30,9.653
2001,7200,50,11.21

#Example output for the above input:
Pack Number: 1
1001,6200,30,9.653
2001,7200,10,11.21
Pack Length: 7200, Pack Weight: 401.69

Pack Number: 2
2001,7200,40,11.21
Pack Length: 7200, Pack Weight: 448.4
```

## Running the application

Prerequisite: [Rust](https://www.rust-lang.org/tools/install) must have been installed when running this app.

- From a terminal in the project-root: ```cargo run```
- From IntelliJ or CLion: select the ```Cargo Run``` configuration

Then enter the data, finish with a blank line.
